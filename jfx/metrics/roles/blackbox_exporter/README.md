# Ansible blackbox_exporter role

A role to install/update Blackbox exporter on Ubuntu and Debian.

[![Ansible Galaxy](https://shields.io/badge/Ansible_Galaxy-informational?logo=ansible&style=flat-square)](https://galaxy.ansible.com/jfx/system) Ansible Galaxy collection.

## Getting Started

### Requirements

In order to use:

* Minimal Ansible version: 2.10

### Dependencies

* `jfx.common.install_opt` role for pre-installation of Blackbox exporter.

### Installation

* Download the `jfx.metrics` collection:

```shell
ansible-galaxy collection install jfx.metrics
```

* Then use the role from the collection in the playbook:

example:

```yaml
  ...
  roles:
    - role: jfx.common.install_opt
      vars:
        in_dir_product: blackbox_exporter
        in_dir_version: "{{ blackbox_exporter_version }}"
        in_dir_package_name: blackbox_exporter-{{ blackbox_exporter_version }}.linux-{{ arch }}
        in_dir_package_ext: tar.gz
        in_dir_download_link: https://github.com/prometheus/blackbox_exporter/releases/download/v{{ blackbox_exporter_version }}/{{ in_dir_package_name }}.{{ in_dir_package_ext }}
        in_dir_temp_dir_keep: true

    - role: jfx.metrics.blackbox
      vars:
        blackbox_version: "{{ blackbox_exporter_version }}"
        blackbox_arch: "{{ arch }}"
```

### blackbox role variables

| Variables     | Description                                                    | Default      |
| ------------- | -------------------------------------------------------------- | ------------ |
| `blackbox_version` | Version of Blackbox exporter. Example: `0.22.0`                | **Required** |
| `blackbox_arch`    | Binary architecture of Blackbox exporter: `amd64`, `arm64` ... | `amd64`      |

### blackbox_exporter files

* `blackbox.service`:
A default `blackbox.service` systemd file is defined but could be overridden by a file located in `{{ playbook_dir }}/files/` directory.

* `blackbox.yml`:
The default `blackbox.yml` file is deployed but could be overridden by a file located in `{{ playbook_dir }}/files/` directory.

## Authors

* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
