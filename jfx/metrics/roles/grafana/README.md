# Ansible Grafana role

A role to install/update Grafana on Ubuntu and Debian.

[![Ansible Galaxy](https://shields.io/badge/Ansible_Galaxy-informational?logo=ansible&style=flat-square)](https://galaxy.ansible.com/jfx/system) Ansible Galaxy collection.

## Getting Started

### Requirements

In order to use:

* Minimal Ansible version: 2.10

### Dependencies

*

### Installation

* Download the `jfx.metrics` collection:

```shell
ansible-galaxy collection install jfx.metrics
```

* Then use the role from the collection in the playbook:

example:

```yaml
  ...
  roles:
    - role: jfx.metrics.grafana
```

### grafana role variables

| Variables              | Description                        | Default             |
| ---------------------- | ---------------------------------- | ------------------- |
| `grafana_keyrings_dir` | Apt keyrings directory path.       | `/etc/apt/keyrings` |
| `grafana_version`      | Define Grafana version to install. | `latest`            |

### grafana files

* `grafana.ini`:
A default `grafana.ini` configuration file is defined but could be overridden by a `grafana.ini.j2` file located in `{{ playbook_dir }}/templates/` directory.

## Authors

* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
