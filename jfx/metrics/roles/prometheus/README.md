# Ansible prometheus role

A role to install/update [Prometheus](https://prometheus.io/) on Ubuntu and Debian.

[![Ansible Galaxy](https://shields.io/badge/Ansible_Galaxy-informational?logo=ansible&style=flat-square)](https://galaxy.ansible.com/jfx/system) Ansible Galaxy collection.

## Getting Started

### Requirements

In order to use:

* Minimal Ansible version: 2.10

### Dependencies

* `jfx.common.install_opt` role for pre-installation of Prometheus.

### Installation

* Download the `jfx.metrics` collection:

```shell
ansible-galaxy collection install jfx.metrics
```

* Then use the role from the collection in the playbook:

example:

```yaml

  ...
  roles:
    - role: jfx.common.install_opt
      vars:
        in_dir_product: prometheus
        in_dir_version: "{{ prometheus_version }}"
        in_dir_package_name: prometheus-{{ prometheus_version }}.linux-{{ arch }}
        in_dir_package_ext: tar.gz
        in_dir_download_link: https://github.com/prometheus/prometheus/releases/download/v{{ prometheus_version }}/{{ in_dir_package_name }}.{{ in_dir_package_ext }}
        in_dir_temp_dir_keep: true

    - role: jfx.metrics.prometheus
      vars:
        prom_version: "{{ prometheus_version }}"
        prom_arch: "{{ arch }}"
```

### prometheus role variables

| Variables      | Description                                             | Default      |
| -------------- | ------------------------------------------------------- | ------------ |
| `prom_version` | Version of Prometheus. Example: `2.40.1`                | **Required** |
| `prom_arch`    | Binary architecture of Prometheus: `amd64`, `arm64` ... | `amd64`      |

### prometheus files

* `prometheus.service`:
A template file to configure prometheus systemctl service could be defined in the `{{ playbook_dir }}/templates/` directory with the name `prometheus.service.j2`. Otherwise a static file `prometheus.service` could be defined in the `{{ playbook_dir }}/files/` directory.

Example of a default service file `prometheus.service` to put in `{{ playbook_dir }}/files/`:

```shell
[Unit]
Description=Prometheus
Documentation=https://prometheus.io/docs/introduction/overview/
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User=prometheus
Group=prometheus
ExecReload=/bin/kill -HUP \$MAINPID
ExecStart=/usr/local/bin/prometheus \
  --config.file=/etc/prometheus/prometheus.yml \
  --storage.tsdb.path=/var/lib/prometheus \
  --web.console.templates=/etc/prometheus/consoles \
  --web.console.libraries=/etc/prometheus/console_libraries \
  --web.listen-address=0.0.0.0:9090 \
  --web.external-url=

SyslogIdentifier=prometheus
Restart=always

[Install]
WantedBy=multi-user.target
```

* `prometheus.yml`:
A default `prometheus.yml.j2` config file is defined but could be overridden by a file located in `{{ playbook_dir }}/templates/` directory.

## Authors

* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
