# Ansible collection Metrics

[![Software License](https://img.shields.io/badge/license-MIT-informational.svg?style=for-the-badge)](LICENSE)
[![semantic-release: angular](https://img.shields.io/badge/semantic--release-angular-e10079?logo=semantic-release&style=for-the-badge)](https://github.com/semantic-release/semantic-release)
[![Pipeline Status](https://img.shields.io/gitlab/pipeline-status/op_so/ansible/metrics?style=for-the-badge)](https://gitlab.com/op_so/ansible/metrics/pipelines)

An [Ansible](https://www.ansible.com/) collection that deploys metrics tools on Ubuntu and Debian.

[![GitLab](https://shields.io/badge/Gitlab-informational?logo=gitlab&style=flat-square)](https://gitlab.com/op_so/ansible/metrics) The main repository.

[![Ansible Galaxy](https://shields.io/badge/Ansible_Galaxy-informational?logo=ansible&style=flat-square)](https://galaxy.ansible.com/jfx/metrics) Ansible Galaxy collection.

This collections includes:

| Roles                                        | Description                                                                            |
| -------------------------------------------- | -------------------------------------------------------------------------------------- |
| [blackbox_exporter](#blackbox_exporter-role) | A role that installs/updates Blackbox exporter on Ubuntu and Debian Operating Systems. |
| [grafana](#grafana-role)                     | A role that installs/updates Grafana on Ubuntu and Debian Operating Systems.           |
| [prometheus](#prometheus-role)               | A role that installs/updates Prometheus on Ubuntu and Debian Operating Systems.        |

## blackbox_exporter role

A role that installs/updates [Blackbox exporter](https://github.com/prometheus/blackbox_exporter) on Ubuntu and Debian Operating Systems.

### blackbox_exporter role dependencies

- `jfx.common.in_dir` role for pre-installation of Prometheus.

### blackbox_exporter role variables

| Variables                   | Description                                                    | Default      |
| --------------------------- | -------------------------------------------------------------- | ------------ |
| `blackbox_exporter_version` | Version of Blackbox exporter. Example: `0.22.0`                | **Required** |
| `blackbox_exporter_arch`    | Binary architecture of Blackbox exporter: `amd64`, `arm64` ... | `amd64`      |

### blackbox_exporter files

- `blackbox_exporter.service`:
A default `blackbox_exporter.service` systemd file is defined but could be overridden by a file located in `{{ playbook_dir }}/files/` directory.

## Grafana role

A role that installs/updates [Grafana](https://grafana.com/oss/grafana/) on Ubuntu and Debian Operating Systems.

### Grafana role dependencies

-

### Grafana role variables

| Variables              | Description                        | Default             |
| ---------------------- | ---------------------------------- | ------------------- |
| `grafana_keyrings_dir` | Apt keyrings directory path.       | `/etc/apt/keyrings` |
| `grafana_version`      | Define Grafana version to install. | `latest`            |

### Grafana files

- `grafana.ini`:
A default `grafana.ini` configuration file is defined but could be overridden by a `grafana.ini.j2` file located in `{{ playbook_dir }}/templates/` directory.

## Prometheus role

A role that installs/updates [Prometheus](https://prometheus.io/) on Ubuntu and Debian Operating Systems.

### Prometheus role dependencies

- `jfx.common.in_dir` role for pre-installation of Prometheus.

### Prometheus role variables

| Variables            | Description                                             | Default      |
| -------------------- | ------------------------------------------------------- | ------------ |
| `prometheus_version` | Version of Prometheus. Example: `2.40.1`                | **Required** |
| `prometheus_arch`    | Binary architecture of Prometheus: `amd64`, `arm64` ... | `amd64`      |

### Prometheus files

- `prometheus.service`:
A template file to configure prometheus systemctl service could be defined in the `{{ playbook_dir }}/templates/` directory with the name `prometheus.service.j2`. Otherwise a static file `prometheus.service` could be defined in the `{{ playbook_dir }}/files/` directory.

Example of a default service file `prometheus.service` to put in `{{ playbook_dir }}/files/`:

```shell
[Unit]
Description=Prometheus
Documentation=https://prometheus.io/docs/introduction/overview/
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User=prometheus
Group=prometheus
ExecReload=/bin/kill -HUP \$MAINPID
ExecStart=/usr/local/bin/prometheus \
  --config.file=/etc/prometheus/prometheus.yml \
  --storage.tsdb.path=/var/lib/prometheus \
  --web.console.templates=/etc/prometheus/consoles \
  --web.console.libraries=/etc/prometheus/console_libraries \
  --web.listen-address=0.0.0.0:9090 \
  --web.external-url=

SyslogIdentifier=prometheus
Restart=always

[Install]
WantedBy=multi-user.target
```

- `prometheus.yml`:
A default `prometheus.yml.j2` config file is defined but could be overridden by a file located in `{{ playbook_dir }}/templates/` directory.

## Getting Started

### Requirements

To use:

- Minimal Ansible version: 2.10

### Installation

- Download the `jfx.common` and `jfx.metrics` collection:

```shell
ansible-galaxy collection install jfx.common
ansible-galaxy collection install jfx.metrics
```

- Then use the roles from the collection in the playbook:

example:

```yaml
  ...
  roles:
    - role: jfx.common.install_opt
      vars:
        in_dir_product: prometheus
        in_dir_version: "{{ prometheus_version }}"
        in_dir_package_name: prometheus-{{ prometheus_version }}.linux-{{ arch }}
        in_dir_package_ext: tar.gz
        in_dir_download_link: https://github.com/prometheus/prometheus/releases/download/v{{ prometheus_version }}/{{ in_dir_package_name }}.{{ in_dir_package_ext }}
        in_dir_temp_dir_keep: true

    - role: jfx.metrics.prometheus
      vars:
        prometheus_version: "{{ prometheus_version }}"
        prometheus_arch: "{{ arch }}"

    - role: jfx.common.install_opt
      vars:
        in_dir_product: blackbox_exporter
        in_dir_version: "{{ blackbox_exporter_version }}"
        in_dir_package_name: blackbox_exporter-{{ blackbox_exporter_version }}.linux-{{ arch }}
        in_dir_package_ext: tar.gz
        in_dir_download_link: https://github.com/prometheus/blackbox_exporter/releases/download/v{{ blackbox_exporter_version }}/{{ in_dir_package_name }}.{{ in_dir_package_ext }}
        in_dir_temp_dir_keep: true

    - role: jfx.metrics.blackbox_exporter
      vars:
        blackbox_exporter_version: "{{ blackbox_exporter_version }}"
        blackbox_exporter_arch: "{{ arch }}"

    - role: jfx.metrics.grafana
      vars:
        grafana_version: 9.5.3
```

## Authors

- **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
